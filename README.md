###The package has 2 parts:



/bim-test: The angular application - start with: `ng serve`



/bim-server: An Express server to serve the mock requests - starts with: `node server`

The git does not contain the node_modules: you have to install the packages with - `npm install` for both the server and the client





###USER Notes:
-You have to Load and Save to server manually on /view page.


-You have to fill out every field to add an Address to the user.


-You have to add an Adress and select a primary address in order to save to users.



###The code is not perfect, many improvements could be made, such as better user-server communication, better form validations, message/error handling, tests, etc.. If these things are required as well let me know and I will add it.


@Created By: Peter Horvath