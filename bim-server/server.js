const express = require('express');
const app = express();
const port = 3000;
var bodyParser = require('body-parser');
app.use(bodyParser.json());

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

let mockUsers = [{
    id: 0,
    name: 'Ali Tucker',
    birthdate: '1934.01.01.',
    phone: '+36202324346',
    primaryAddressIdx: 0,
    addresses: [
        {   
            id: 0,
            ZIP: '1011',
            city: 'Budapest',
            address: 'Váci út 2.'
        }
        ]
    },
{
    id: 1,
    name: 'Tom Tomson',
    birthdate: '1956.11.11.',
    phone: '+400019876541',
    primaryAddressIdx: 0,
    addresses: [
        {
            id: 0,
            ZIP: '1119',
            city: 'Budapest',
            address: 'Fehérvári út. 6.'
        }
        ],
    },
    {
        id: 2,
        name: 'Bob Tucker',
        birthdate: '1967.10.08.',
        phone: '+36302222222',
        primaryAddressIdx: 0,
        addresses: [
            {
                id: 0,
                ZIP: '1000',
                city: 'Budapest',
                address: 'Petőfi u. 9.'
            },
            {
                id: 1,
                ZIP: '1090',
                city: 'Budapest',
                address: 'Wesselényi u. 7.'
            }
            ]
    },
    {
        id: 3,
        name: 'Tom Johns',
        birthdate: '1922.10.08.',
        phone: '+3630213242',
        primaryAddressIdx: 0,
        addresses: [
            {
                id: 0,
                ZIP: '1013',
                city: 'Budapest',
                address: 'Király u. 10.'
            }
            ]
    }];

app.get('/', (req, res) => res.send(JSON.stringify({ a: 1})));

app.get('/users', (req, res) => res.send(JSON.stringify( mockUsers )));

app.post('/users/new ', function ( req,res,next) {
    console.log('User created');
});

app.post('/users/save', function ( req,res,next) {
    mockUsers = req.body;
});


app.delete('/users/:id', function ( req, res) {
    console.log('Deleting User with ID: ' + req.params.id);
    res.send('Succesfully deleted');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));