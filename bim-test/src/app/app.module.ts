import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ViewUsersComponent } from './view-users/view-users.component';
import { UserRowComponent } from './user-row/user-row.component';
import { HttpClientModule } from '@angular/common/http';
import { ActionPanelComponent } from './action-panel/action-panel.component';
import { EditComponent } from './edit/edit.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

const routes: Routes = [
  { path: '', redirectTo: '/view', pathMatch: 'full' },
  { path: 'view', component: ViewUsersComponent },
  { path: 'user/:id', component: EditComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ViewUsersComponent,
    UserRowComponent,
    ActionPanelComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    AngularFontAwesomeModule
  ],
  exports: [ RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
