import { Component, OnInit, ApplicationRef } from '@angular/core';
import { UserService } from '../services/user.service';
import { User } from '../models/user';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Address } from '../models/address';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  user: User;
  userForm: FormGroup;
  addressForm: FormGroup;

  constructor( private userService: UserService, private route: ActivatedRoute, private location: Location, private router: Router, private formBuilder: FormBuilder) { 
  }

  ngOnInit() {

    this.userForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      birthdate: ['', [Validators.required]],
      phone: ['', [Validators.required]],
      pAddress: [-1, Validators.min(0)]
    });

    this.addressForm = this.formBuilder.group({
      zip: ['', Validators.required],
      city: ['', Validators.required],
      address: ['', Validators.required]
    });

    this.getUser();
  }

  getUser() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.user = this.userService.getUser(id);
    if (this.user == null) {
      this.user = new User();
      this.user.id = -1;
    } else {
      this.userForm.setValue({
        name: this.user.name,
        birthdate: this.user.birthdate,
        phone: this.user.phone,
        pAddress: this.user.primaryAddressIdx
      })
    }
  }

  ngAfterViewInit(){
    
  }

  addUser() {
    let usr = new User();
    this.userService.addNewUser(usr);
  }

  saveUser(){
    
    this.user.name = this.userForm.get('name').value;
    this.user.phone = this.userForm.get('phone').value;
    this.user.birthdate = this.userForm.get('birthdate').value;
    this.user.primaryAddressIdx = this.userForm.get('pAddress').value;
    
    if (this.user.id == -1){
      this.userService.addNewUser(this.user);
    } else {
      this.userService.modifyUser(this.user);
    }
    this.router.navigateByUrl('/view');
  }


  cancel() {
    this.location.back();
  }

  addAddress() {
    let maxIdx = -1;
    let tmpAddress = new Address();
    for ( let x of this.user.addresses) {
      if ( x.id > maxIdx ) {
        maxIdx = x.id;
      }
    }
    tmpAddress.id = maxIdx + 1;
    tmpAddress.address = this.addressForm.get('address').value;
    tmpAddress.city = this.addressForm.get('city').value;
    tmpAddress.ZIP = this.addressForm.get('zip').value;
    this.user.addresses.push(tmpAddress);
    this.addressForm.reset();
  }

  submit($event) {
    $event.preventDefault();
  }
}
