import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  cachedUsers: User[] = [];
  usersSubject: Subject<any> = new Subject<any>();
  url = 'http://localhost:3000/users';
  constructor( private http: HttpClient) { }

  getUsers() {
    this.http.get(this.url).subscribe( result =>
      {
        let tmp = JSON.parse(JSON.stringify(result));
        this.cachedUsers = [];
        for (let item of tmp) {
          let u = new User;
          u.copyInto(item);
          this.cachedUsers.push(u);
        }
        this.usersSubject.next(this.cachedUsers);
      });
      return this.usersSubject.asObservable();
  }

  getUser(id) {
    for (let i = 0; i < this.cachedUsers.length; i++) {
      if ( this.cachedUsers[i].id == id) {
        return this.cachedUsers[i];
      }
    }
    return null;
  }

  modifyUser(user: User) {
    for (let i = 0; i < this.cachedUsers.length; i++) {
      if (user.id == this.cachedUsers[i].id) {
        this.cachedUsers[i] = user;
        break;
      }
    }
  }

  addNewUser(user: User) {
    user.id = this.cachedUsers[this.cachedUsers.length-1].id +1;
    this.cachedUsers.push(user);
    this.usersSubject.next(this.cachedUsers);
  }

  addUserToCache( user: User) {

  }

  deleteUsers(id: number) {
    return this.http.delete(this.url + 'id');
  }

  saveUsers() {
    return this.http.post(this.url + '/save', this.cachedUsers);
  }

}
