import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-action-panel',
  templateUrl: './action-panel.component.html',
  styleUrls: ['./action-panel.component.scss']
})
export class ActionPanelComponent implements OnInit {

  constructor( private userService: UserService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

  loadUsers() {
    this.userService.getUsers().subscribe( res=> console.log('loading users'));
  }

  saveUsers() {
    this.userService.saveUsers().subscribe( res => console.log('users are being saved'));
  }

  newUser(){
    let newU ={
      id: 3,
      name: 'Jim Bob JimmyBob',
      birthdate: '191412',
      phone: '012321246',
      primaryAddress: {
          ZIP: '1111',
          City: 'BP',
          Address: 'Street 99.'
      }
    };
    this.router.navigateByUrl('/user/-1');
  }

}
