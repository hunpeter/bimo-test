import { Component, OnInit, Input, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: '[app-user-row]',
  templateUrl: './user-row.component.html',
  styleUrls: ['./user-row.component.scss']
})
export class UserRowComponent implements OnInit {

  @Input() user;
  @Output() deleteEvent = new EventEmitter();

  constructor( private router: Router, private route: ActivatedRoute ) { }

  ngOnInit() {
  }

  deleteUser(){
    this.deleteEvent.emit(this.user);
  }

  editUser() {
    this.router.navigateByUrl('/user/'+this.user.id);
  }

}
