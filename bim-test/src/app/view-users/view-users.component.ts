import { Component, OnInit } from '@angular/core';
import { User } from '../models/user';
import { UserService } from '../services/user.service';
import { MockUsers } from '../models/mockUsers';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.scss']
})
export class ViewUsersComponent implements OnInit {

  users: any;
  sorting: any;
  filter = "";

  constructor( private userService: UserService) {
    this.userService.usersSubject.subscribe( res => {
      this.users = res;
      this.sortArray();
    });
    this.sorting = null;
   }

  ngOnInit() {
    if ( this.userService.cachedUsers.length > 0 ){
      this.users = this.userService.cachedUsers;
    } else {
      this.userService.getUsers();
    }
  }

  deleteUser(user) {
    let deleteIdx: number = null;
    // Get the user
    for (let i = 0; i < this.users.length; i++ ){
      if (user.id == this.users[i].id) {
        deleteIdx = i;
        break;
      }
    }
    if ( deleteIdx != null ){
      this.users.splice(deleteIdx,1);
    }
  }

  sortToggle(paramName: string) {
    if ( this.sorting == null || this.sorting.param != paramName) {
      this.sorting = { value: 'ASC', param: paramName, d: 'sort-down' }; 
      this.sortArray();
    } else {
      this.sorting.value = this.sorting.value == 'ASC' ? 'DESC' : 'ASC';
      this.sortArray();
    }
  }

  sortArray(){
    if (this.sorting) {
      let param = this.sorting.param;
      if (this.sorting.value == 'ASC') {
        this.users = this.users.sort( (a,b) => { 
          if ( a[param] < b[param]) return -1;
          if ( a[param] > b[param]) return 1;
          return 0;
        });
      } else {
        this.users = this.users.sort( (a,b) => { 
          if ( a[param] < b[param]) return 1;
          if ( a[param] > b[param]) return -1;
          return 0;
        });
      }
    }
  }

  filterUsers() {
    let keyword = this.filter;
    this.users = this.userService.cachedUsers.filter( u => {
      return u.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1 });
  }
}
