export const MockUsers: any[] = 
[
    {
        id: 0,
        name: 'Ali Tucker',
        birthdate: '19911212',
        phone: '012324346',
        primaryAddress: {
            ZIP: '1212',
            City: 'BP',
            Address: 'Street 12.'
        }
    },
    {
        id: 1,
        name: 'Tom Tomson',
        birthdate: '191254323',
        phone: '9876541',
        primaryAddress: {
            ZIP: '1232',
            City: 'BP',
            Address: 'Street 16.'
        },
        additionalAddress: [
            {
                    ZIP: '1111',
                    City: 'BP',
                    Address: 'Street 14.'
            }
        ]
    }
]