export class Address {
    id: number;
    ZIP: String;
    city: String;
    address: String;

    constructor() {
        this.ZIP = '';
        this.city = '';
        this.address = '';
    }

    copyInto(obj: any) {
        this.id = obj.id;
        this.address = obj.address;
        this.ZIP = obj.ZIP;
        this.city = obj.city;
    }
}
