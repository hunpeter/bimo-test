import { Address } from './address';
export class User {
    id: number;
    name: String;
    birthdate: any;
    phone: string;
    primaryAddressIdx: number;
    addresses: Address[];

    constructor() {
        this.addresses = [];
        this.primaryAddressIdx = -1;
    }

    copyInto( obj: any) {
        this.id = obj.id;
        this.name = obj.name;
        this.birthdate = obj.birthdate;
        this.phone = obj.phone;
        this.primaryAddressIdx = obj.primaryAddressIdx;
        for( let adr of obj.addresses) {
            let tmp = new Address();
            tmp.copyInto(adr);
            this.addresses.push(tmp);
        }
    }
}
